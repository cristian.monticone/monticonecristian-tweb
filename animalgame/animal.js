var id, yes_id, no_id;

$(function() {
  fetch(1);

  $("#yes").click( function() {
    if (yes_id === "0") theEnd(this.id);
    else fetch(yes_id);
  });
  $("#no").click( function() {
    if (no_id === "0") theEnd(this.id);
    else fetch(no_id);
  });
});

function fetch(id) {
  $.ajax({
    url: "fetch_answer.php",
    type: "GET",
    data: {
      "id": id
    },
    success: function(result) {
      id = result.id;
      yes_id = result.yes_id;
      no_id = result.no_id;
      $("#answer h1").html(result.answer);
  }});
}

function theEnd(id) {
  var message = (id === "yes") ? "Ho indovinato! Gioca di nuovo!" : "Ho fatto del mio meglio ma ho fallito...";
  $("button").hide();
  $("#answer h1").html(message);
}
