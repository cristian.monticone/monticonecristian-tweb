import java.io.*;
import java.net.*;
import java.util.regex.*;
import java.util.concurrent.*;
import java.util.*;

public class Fetcher implements Runnable {
  private HashSet<Integer> memo;
  private ExecutorService pool;
  private PrintWriter out;
  private int id;

  public Fetcher(int id, HashSet<Integer> memo, PrintWriter out, ExecutorService pool) {
    this.memo = memo;
    this.id = id;
    this.pool = pool;
    this.out = out;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void run() {
    if (id == 0) return;
    synchronized(memo) {
      if (memo.contains(id)) return;
      memo.add(id);  
      System.out.println(memo.size());
    }

    BufferedReader in = null;

    String answer = "ERROR FETCHING ANSWER";
    Integer yesId = 0;
    Integer noId = 0;
    try {
      URL target = new URL("http://animalgame.com/play/index.php?id=" + id);

      in = new BufferedReader( new InputStreamReader(target.openStream()));

      String page = "";
      String line;

      while ((line = in.readLine()) != null) {
        page += line;
      }

      Pattern p = Pattern.compile("<FONT SIZE=\\+3> (.*) </FONT>");
      Matcher m = p.matcher(page);
      if (m.find()) {
        answer = m.group(1).trim();
      }

      p = Pattern.compile("<FORM METHOD=POST ACTION=\"/play/index.php\\?id=([0-9]*)\">        <INPUT TYPE=\"SUBMIT\" NAME=\"YES\" VALUE=\"yes\">");
      m = p.matcher(page);
      if (m.find()) {
        yesId = Integer.parseInt(m.group(1).trim());
      }

      p = Pattern.compile("<FORM METHOD=POST ACTION=\"/play/index.php\\?id=([0-9]*)\">        <INPUT TYPE=\"SUBMIT\" NAME=\"NO\" VALUE=\" no \">");
      m = p.matcher(page);
      if (m.find()) {
        noId = Integer.parseInt(m.group(1).trim());
      }

      synchronized(out) {
        out.println(id + ",\"" + toHTML(answer) + "\"," + yesId + "," + noId);
        out.flush();
      }
    }
    catch(IOException e) { System.out.println("Error: " + e.getMessage()); }
    finally { try { if (in != null) in.close(); } catch(IOException e) {} }

    Fetcher yesFetcher = new Fetcher(yesId, memo, out, pool);
    Fetcher noFetcher = new Fetcher(noId, memo, out, pool);

    synchronized(pool) {
      pool.execute(yesFetcher);
      pool.execute(noFetcher);
    }
  }

  private String toHTML(String str) {
    String out = "";
    for (char c: str.toCharArray()) {
      if(!Character.isLetterOrDigit(c) && c != ' ' && c != '?' && c != '!')
        out += String.format("&#x%x;", (int)c);
      else
        out += String.format("%s", c);

    }
    return out;
  }

}
