import java.io.*;
import java.net.*;
import java.util.concurrent.*;
import java.util.*;

public class Main {
  final static int MAX_CONCURRENT_CONNECTIONS = 100;
  final static String CSV_PATH = "data.csv";

  public static void main(String[] args) {
    ExecutorService pool = Executors.newFixedThreadPool(MAX_CONCURRENT_CONNECTIONS);
    HashSet<Integer> memo = new HashSet<>();
    PrintWriter out = null;

    try {
      out = new PrintWriter(new FileWriter(CSV_PATH));
    }
    catch(IOException ioe) {
      System.out.println("Error opening '" + CSV_PATH +"': " + ioe.getMessage());
      return;
    }

    Fetcher f = new Fetcher(1, memo, out, pool);

    pool.execute(f);

    // Some weird control to check if all is finished.
    int count;
    boolean flag = true;
    synchronized(memo) {
      count = memo.size();
    }
    while(flag) {
      try {
        Thread.sleep(2000);
      }
      catch(InterruptedException ie) {
        System.out.println("Interrupted: " + ie.getMessage());
        System.out.println("Continuing...");
      }
      synchronized(memo) {
        flag = (count != memo.size());
        count = memo.size();
      }
    }

    System.out.println("Crowling seems to be finished, stopping process...");
    pool.shutdownNow();
  }
}
