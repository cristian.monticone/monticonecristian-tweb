<?php
  header("Content-type: application/json");

  $id = htmlspecialchars($_GET['id']);

  // DB connection block
  try {
    $dbh = new PDO("mysql:host=localhost;dbname=animal_tree", "user", "3yhgtg4ie4");

    $query = "SELECT * FROM decision WHERE id = " . $dbh->quote($id) . ";";
    $query_result = $dbh->query($query);
  } 
  catch (PDOexception $e) {
    header("HTTP/1.1 500 Internal Server Error");
    die("An HTTP error 500 (internal server error) occurred.");
  }

  if ($query_result->rowCount() == 0) {
    header("HTTP/1.1 404 File Not Found");
    die("An HTTP error 404 (file not found) occurred.");
  }

  $aux = $query_result->fetch();

  $json_arr = array(
    'id' => $aux['id'],
    'answer' => $aux['answer'],
    'yes_id' => $aux['yes_id'],
    'no_id' => $aux['no_id']
  );

  echo json_encode($json_arr);
?>
