<?php include("top.html"); ?>

<div>
  <form method="post" action="signup-submit.php">
    <fieldset>
      <legend>New User Singup:</legend>
      <ul>
        <li><strong><label class="left">Name:</label></strong><input type="text" name="name"></li>
        <li>
            <strong><label class="left">Gender:</label></strong>
            <input type="radio" name="gender" value="male">Male</input>
            <input type="radio" name="gender" value="female">Female</input>
        </li>
        <li><strong><label class="left">Age:</label></strong><input type="text" maxlength="3" size="6" name="age"></li>
        <li>
          <strong><label class="left">Personality type:</label></strong>
          <input type="text" maxlength="6" size="6" name="personality">
          (<a href="http://www.humanmetrics.com/cgi-win/JTypes2.asp">Don't know your type?</a>)
        </li>
        <li>
            <strong><label class="left">Favorite OS:</label></strong>
            <select name="os">
              <option value="Windows">Windows</option>
              <option value="Mac OS X">Mac OS X</option>
              <option value="Linux" selected>GNU/Linux</option>
            </select>
        </li>
        <li>
          <strong><label class="left">Seeking age:</label></strong>
          <input type="text" maxlength="3" size="6" name="age-from">
          to
          <input type="text" maxlength="3" size="6" name="age-to">
        </li>
      </ul>
      <input type="submit" value="Sign Up">
  </form>
</div>

<?php include("bottom.html"); ?>
