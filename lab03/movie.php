<!--
Nome: Cristian
Cognome: Monticone
Matricola: 842201
Corso: Tecnologie Web (MFN0634)
Esercizio: lab03
-->
<!DOCTYPE html>

<?php
/**
 * It takes a string and try to clean it from starting/ending spaces
 *
 * @param $val The value to be cleaned
 * @return The cleaned value 
 */
function trim_value(&$val) {
  $val = trim($val);
}

/**
 * It prints an entry for the overview panel
 *
 * @param $film_overview The array with all the overview infos
 * @param $info The name of the oververview element
 */
function print_overview_info($film_overview, $info) {
  if ($film_overview[$info]) {
    echo "<dt>$info</dt>";
    echo "<dd>{$film_overview[$info]}</dd>";
  }
}

/**
 * It prints a set of reviews in HTML
 *
 * @param $reviews_array An array of reviews
 */
function print_reviews($reviews_array) {
  foreach ($reviews_array as $review) {
    ?>
      <p class="reviews-quotation">
        <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/<?= strtolower($review['rate']) ?>.gif" alt="Rotten">
        <q><?= $review['text'] ?></q>
      </p>
      <p class="reviews-critic">
        <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/critic.gif" alt="Critic">
        <?= $review['user'] ?><br>
        <span class="critic-pubblication"><?= $review['company'] ?></span>
      </p>
    <?php
  }
}

// Maximum reviews for page
$MAX_PAGE_REVIEWS = 10;

// Loading film infos
$film = $_GET['film'];

// Default film if no movie was selected
if (empty($film)) $film = "tmnt";

$film_info = file("$film/info.txt");

$film_overview_lines = file("$film/overview.txt");

// Making overviews dictionary PROPERTY => VALUE
foreach ($film_overview_lines as $record) {
  $aux = explode(":", $record);

  $film_overview[$aux[0]] = $aux[1];
}

// Making reviews array
$reviews = array();
foreach (glob("$film/review*.txt") as $review_file) {
  $lines = file($review_file);

  // Cleaning from spaces from start/end of lines
  array_walk($lines, 'trim_value');

  $record = array('text' => $lines[0],
                  'rate' => $lines[1],
                  'user' => $lines[2], 
                  'company' => $lines[3]);

  array_push($reviews, $record);
}

// Cleaning data from blank spaces before/after the values
array_walk($film_info, 'trim_value');
array_walk($film_overview, 'trim_value');

// In some films overviews txt file 'MOVIE SYNOPSIS' can be only 'SYNOPSIS'
if ($film_overview['SYNOPSIS'])
$film_overview['MOVIE SYNOPSIS'] = $film_overview['SYNOPSIS'];
?>

<html lang="en">
  <head>
    <title>Rancid Tomatoes</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="https://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/rotten.gif" type="image/gif" rel="shortcut icon">
    <link href="movie.css" type="text/css" rel="stylesheet">
  </head>

  <body>
    <div id="banner">
      <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/banner.png" alt="Rancid Tomatoes">
    </div>

    <h1 id="film-title"><?= $film_info[0] ?> (<?= $film_info[1] ?>)</h1>

    <div id="main">
      <div id="overview">
        <div>
          <img src="<?= $film ?>/overview.png" alt="general overview">
        </div>

        <dl>

        <?php
        // Printing overview entries
        print_overview_info($film_overview, "STARRING");
        print_overview_info($film_overview, "DIRECTOR");
        print_overview_info($film_overview, "RATING");
        print_overview_info($film_overview, "THEATRICAL RELEASE");
        print_overview_info($film_overview, "MOVIE SYNOPSIS");
        print_overview_info($film_overview, "MPAA RATING");
        print_overview_info($film_overview, "RELEASE COMPANY");
        print_overview_info($film_overview, "RUNTIME");
        print_overview_info($film_overview, "GENRE");
        print_overview_info($film_overview, "BOX OFFICE");
        print_overview_info($film_overview, "LINKS");
        ?>

        </dl>
      </div>

      <div id="reviews">
        <div id="reviews-header">
          <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/<?= ($film_info[2] >= 60 ) ? "fresh" : "rotten" ?>big.png" alt="Rotten">
          <span><?= $film_info[2] ?>%</span>
        </div>

        <div class="reviews-col">
          <?php
          // Pre-calculating the offset for the array slicing
          $offset = round(count($reviews)/2);
          if ($offset > 5) $offset = 5;

          // Slising the reviews array in two
          $left_reviews = array_slice($reviews, 0, $offset);
          $right_reviews = array_slice($reviews, $offset, $MAX_PAGE_REVIEWS - $offset);

          print_reviews($left_reviews);
          ?>
        </div>

        <div class="reviews-col">
          <?php
          print_reviews($right_reviews);
          ?>
        </div>
      </div>

      <p id="footer">
        (1-<?= (count($reviews) > 10) ? 10 : count($reviews) ?>) of <?= count($reviews) ?>
      </p>
    </div>

    <div id="validators">
      <a href="http://validator.w3.org/check/referer">
        <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/w3c-xhtml.png" alt="Validate HTML">
      </a>
      <br>
      <a href="http://jigsaw.w3.org/css-validator/check/referer">
        <img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!">
      </a>
    </div>
  </body>
</html>
