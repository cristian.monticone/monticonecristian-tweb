Fans of the book will probably be a little disappointed, but anyone encountering the Hitchhiker's Guide for the first time should be thoroughly entertained. It is very strange and very funny in a way that Adams' work exemplified.
FRESH
Jonathan Rosenbaum
Chicago Reader
