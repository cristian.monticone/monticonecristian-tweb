"The Hitchhiker's Guide To The Galaxy" is an enjoyable sci-fi romp with a strong twist of self-effacing British raillery that children of all ages will get a charge from.
FRESH
Clint Morris
Moviehole
