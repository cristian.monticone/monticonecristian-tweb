<?php 
  include("top.html");
  require_once("mdb_lib.php");

  $first_name = filter_input(INPUT_GET, "firstname");
  $last_name = filter_input(INPUT_GET, "lastname");
?>

<h1>Results for <?= $first_name . " " . $last_name ?></h1>

<p><?= print_table($first_name, $last_name) ?></p>

<?php include("bottom.html"); ?>
