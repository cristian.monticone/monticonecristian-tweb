<?php
  function try_query($query) {
    try {
      $dbh = new PDO("mysql:host=localhost;dbname=imdb", "tweb-user", "tweb-secret-password");
      $query_result = $dbh->query($query);
    } 
    catch (PDOexception $e) {
      print "<h1>Database error: {$e->getMessage()} </h1><br>";
      include("bottom.html");
      die();
    }
   
    return $query_result;
  }


  function print_table($first_name, $last_name, $kevin = false) {
    $actor_id_statement = try_query("SELECT id
                                     FROM actors a
                                     WHERE (first_name LIKE '$first_name (%)' OR first_name = '$first_name') 
                                           AND last_name = '$last_name'
                                     ORDER BY film_count DESC LIMIT 1;");

    if ($actor_id_statement->rowCount() == 0) return "<p>Actor $first_name $last_name not found.</p>";

    $actor_id = $actor_id_statement->fetch()['id'];

    if ($kevin) {
      // Nella prima subquery non faccio il join anche con la tabella actors
      // perchè ho già l'id dell'attore.
      $query = "SELECT DISTINCT name, year
                FROM movies m
                WHERE id IN
                    (SELECT m.id AS id
                     FROM roles r JOIN movies m ON (m.id = r.movie_id)
                     WHERE r.actor_id = $actor_id)
                AND id IN 
                    (SELECT m.id AS id
                     FROM actors a JOIN roles r ON (a.id = r.actor_id)
                     JOIN movies m ON (m.id = r.movie_id)
                     WHERE a.first_name = 'Kevin' AND a.last_name = 'Bacon')
                ORDER BY year DESC;";
    }
    else {
      // Non faccio il join anche con la tabella actors perchè ho già l'id dell'attore.
      $query = "SELECT DISTINCT name, year 
                FROM roles r JOIN movies m ON (r.movie_id = m.id)
                WHERE r.actor_id = $actor_id
                ORDER BY year DESC;";
    }

    $result_query = try_query($query);

    if ($result_query->rowCount() == 0) 
      return "<p>No films found with $first_name $last_name" . (!$kevin ?: " and Kevin Bacon") . ".</p>";

    ?>
      <table id="query_table">
        <caption><?= ($kevin) ? "Films with $first_name $last_name and Kevin Bacon" : "All films" ?></caption>
        <tr>
          <th>#</th>
          <th>Title</th>
          <th>Year</th>
        </tr>
    <?php

      $i = 1;
      foreach ($result_query as $tuple) {
      ?>
        <tr <?= ($i % 2 == 0) ?: "class=\"colored_row\"" ?>>
          <td><?= $i++ ?></td>
          <td><?= $tuple['name'] ?></td>
          <td><?= $tuple['year'] ?></td>
        </tr>
      <?php
      }

    ?>
      </table>
    <?php

  }
?>
