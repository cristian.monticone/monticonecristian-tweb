var puzzleArray;

window.onload = function() {
  initPuzzleArray();

  initPuzzle();

  newGame();
};

function initPuzzle() {
  var divs = $$("#puzzlearea div");
  for (var i = 0; i < divs.length; i++) {
    var x = (i%4);
    var y = Math.floor(i/4);
    divs[i].style.visibility = "visible";
    divs[i].style.backgroundPosition = "-" + x*100 + "px -" + y*100 + "px";
    divs[i].observe("click", tryMove);
    divs[i].observe("mouseover", overDiv);
    divs[i].observe("mouseout", outDiv);
    divs[i].className = "normal-div";
  }
  $("shufflebutton").observe("click", newGame);
}

function newGame() {
  $("win-par").innerHTML = "";
  shuffle();
  refreshPuzzle();
}

function outDiv() {
  this.className = "normal-div";
}

function overDiv() {
  var thisIndex = id2index(parseInt(this.innerHTML));
  if (canMove(thisIndex)) 
    this.className = "hover-div";
}

function initPuzzleArray() {
  puzzleArray = new Array(16);
  for (var i = 0; i < 15; i++) {
    puzzleArray[i] = i+1;
  }
  puzzleArray[15] = 16;
}

function refreshPuzzle() {
  var divs = $$("#puzzlearea div");
  var emptyIndex = id2index(16);
  for (var i = 0; i < divs.length; i++) {
    var divNumber = parseInt(divs[i].innerHTML);
    var index = id2index(divNumber);

    var x = (index%4);
    var y = Math.floor(index/4);
    divs[i].style.top = y*100 + "px";
    divs[i].style.left = x*100 + "px";
  }
}

function tryMove() {
  var id = parseInt(this.innerHTML);
  var thisIndex = id2index(id);
  var emptyIndex = id2index(16);

  if (canMove(thisIndex)) {
    swap(emptyIndex, thisIndex);
    refreshPuzzle();
    checkWin();
  }
}

function canMove(index) {
  var emptyIndex = id2index(16);

  if (emptyIndex === index -1 ||
      emptyIndex === index +1 ||
      emptyIndex === index -4 ||
      emptyIndex === index +4)
    return true;
  else
    return false;
}

function checkWin() {
  var win = true;
  for (var i = 0; win && i < 15; i++) {
    win = (puzzleArray[i] === i+1);
  }

  if (win)
    $("win-par").innerHTML = "Hai vinto!";
  else
    $("win-par").innerHTML = "";
}

function id2index(id) {
  for (var i = 0; i < 16; i++) {
    if (puzzleArray[i] == id)
      return i;
  }
  return -1;
}

function swap(i, j) {
  var tmp = puzzleArray[i];
  puzzleArray[i] = puzzleArray[j];
  puzzleArray[j] = tmp;
}

/**
 * Shuffles the puzzlearea array in place.
 */
function shuffle() {
  initPuzzleArray();
  var j, x, i;
  for (i = puzzleArray.length - 2; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1));
    swap(i, j);
  }
}
