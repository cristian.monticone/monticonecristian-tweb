const STANDARD_FRAME = 1;
const REGISTER_FRAME = 2;
const LOGIN_FRAME = 3;
const ORDER_FRAME = 4;

class Model {
  constructor() {
  }

  init(callback) {
    this.current_p_id = null;
    this.current_p_title = null;
    this.current_p_price = null;


    var model = this;
    apiRequest({
        "request": "session_check"
      },
      function(result) {
        if (result.ok) {
          model.login = true;
          model.username = result.username;
        }
        else {
          model.login = false;
        }

        callback(model);
      }
    );
  
    this.main_frame = STANDARD_FRAME;
  }
}

