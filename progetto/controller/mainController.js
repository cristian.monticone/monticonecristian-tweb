$(document).ready(function() {
  var model = new Model();
  model.init(loadSite);
});

function apiRequest(data, callback) {
  $.ajax({
    url: "api/api.php",
    type: "POST",
    data: data,
    success: callback,
    error: function() { 
      $("#main-frame").load("view/html/api_error.html");
    }
  });
}

function loadSite(model) {
  $("#header").load("view/html/nav.html", function() {navController(model)});

  $("#main-frame").load("view/html/main_frame.html", function() {mainFrameController(model)});
}

function mainFrameController(model, middleFrame = "view/html/home.html") {
  if (model.main_frame != STANDARD_FRAME) {
    model.main_frame = STANDARD_FRAME;
    $("#main-frame").load("view/html/main_frame.html", function() {mainFrameController(model, middleFrame)});
  }
  else {
    $("#category-list").load("view/html/categories.html", function() {categoriesController(model)});
    $("#middle-frame").load(middleFrame);
    $("#shopping-cart-frame").load("view/html/shopping_cart.html", function() {cartController(model)});
  }
}
