function navController(model) {
  boxController(model);

  $("#home-link").click(function() {mainFrameController(model)});

  $("#search-control").on("change paste keyup", function() {searchProduct(model, $(this).val())});
  $("#search-form").submit(function(e) { e.preventDefault() });
}

function boxController(model) {
  if (model.login)
    $("#user-nav-box").load("view/html/user_nav_box.html", 
                            function() {userBoxController(model)});
  else
    $("#user-nav-box").load("view/html/login_nav_box.html",
                            function() {loginBoxController(model)});
}

function userBoxController(model) {
  $("#logout-button").click(function() {logout(model)});
  $("#username-label").replaceWith("Benvenuto/a " + model.username);
}

function logout(model) {
  model.login = false;

  apiRequest({
      "request": "logout"
  });

  $("#main-frame").load("view/html/main_frame.html", function() {mainFrameController(model)});

  $("#user-nav-box").load("view/html/login_nav_box.html",
                          function() {loginBoxController(model)});
}

function loginBoxController(model) {
  $("#login-button").click(function() {
    $("#main-frame").load("view/html/login.html", function() {loginController(model)});
  });

  $("#register-button").click(function() {
    $("#main-frame").load("view/html/register.html", function() {registerController(model)});
  });
}

function searchProduct(model, query) {
  if (model.main_frame != STANDARD_FRAME) {
    model.main_frame = STANDARD_FRAME;
    $("#main-frame").load("view/html/main_frame.html", function() {
      mainFrameController(model);
      searchProduct(model, query);
    });
  }
  else {
    apiRequest({
        "request": "search_products",
        "query": query
      },
      function(result) {
        checkSearchRequest(model, result);
      }
    );
  }
}

function checkSearchRequest(model, result) {
  if (result.ok) {
    if (result.products.length == 0)
      $("#middle-frame").load("view/html/no_result.html");
    else
      $("#middle-frame").load("view/html/product_list.html", function() { loadProductTemplate(model, result);});
  }
  else {
    $("#middle-frame").load("view/html/home.html");
  }
}
