function orderController(model) {
  model.main_frame = ORDER_FRAME;

  var cart_lines = Cookies.getJSON('cart_lines');
  var total_cents = parseInt(Cookies.get('total_cents'));
  loadOrderSummaryTemplate(model, cart_lines, total_cents);

  $("#order-form").submit(function(e) {
    e.preventDefault();
    insertOrder(model, 
          this.elements['address'].value,
          this.elements['code'].value,
          this.elements['ccv'].value,
          this.elements['expiration'].value,
          this.elements['method'].value
    );
  });
}

function loadOrderSummaryTemplate(model, cart_lines, total_cents) {
  $.get(
    "view/html/template/order_summary_entry.html",
    function(data) {loadOrderSummaryEntries(model, cart_lines, total_cents, data);}
  );
}

function loadOrderSummaryEntries(model, cart_lines, total_cents, tmpl_source) {
  // Carico il template
  var template = Handlebars.compile(tmpl_source);

  for (i = 0; i < cart_lines.length; i++) {
    var content = template({
      title: cart_lines[i].title,
      number: cart_lines[i].numero,
      price: (cart_lines[i].prezzo * cart_lines[i].numero / 100)
    });
    $("#order-summary > table > tbody").append(content);
  }

  $("#order-total-price").text(total_cents/100);
}

function insertOrder(model, address, code, ccv, expiration, method) {
  var cart_lines = Cookies.get('cart_lines');

  apiRequest({
      "request": "insert_order",
      "address": address,
      "card_code": code,
      "ccv": ccv,
      "payment_method": method,
      "expiration": "21201",
      "ordered_products": cart_lines
    },
    function(result) {
      insertOrderResult(model, result);
    }
  );
}

function insertOrderResult(model, result) {
  // Flush del carrello.
  if (result.ok) {
    Cookies.remove("cart_lines");
    Cookies.remove("total_cents");

    $("#main-frame").load(
      "view/html/main_frame.html", 
      function() {
        mainFrameController(model, "view/html/order_completed.html");}
    );
  }
  else if (result.error = DEINED_REQUEST_ERROR) {
    $("#user-nav-box").load("view/html/login_nav_box.html",
                            function() {loginBoxController(model)});
    $("#main-frame").load(
      "view/html/main_frame.html", 
      function() {mainFrameController(model, "view/html/not_logged.html");}
    );
  }
  else {
    $("#main-frame").load("view/html/api_error.html");
  }
}
