// Error codes:
const GENERIC_ERROR             = 1;
const USER_NOT_FOUND_ERROR      = 2;
const PASSWORD_NOT_MATCH_ERROR  = 3;
const INVALID_REQUEST_ERROR     = 4;
const NOT_YET_IMPLEMENTED_ERROR = 5;
const INVALID_INPUT_ERROR       = 6;
const INTERNAL_SERVER_ERROR     = 7;
const DUPLICATE_USER_ERROR      = 8;
const DUPLICATE_MAIL_ERROR      = 9;
const NO_ACTIVE_SESSION_ERROR   = 10;
const MAIL_NOT_FOUND_ERROR      = 11;
const DEINED_REQUEST_ERROR      = 12;

function loginController(model) {
  model.main_frame = LOGIN_FRAME;

  $("#login-form").submit(function(e) {
    e.preventDefault();
    login(model, 
          this.elements['username'].value,
          this.elements['password'].value,
          handleLoginResult);
  });
}

function handleLoginResult(model, result) {
  $("#login-form input").removeClass("is-invalid");

  if (result.ok) {
    model.login = true;
    model.username = result.username;

    boxController(model);

    $("#main-frame").load("view/html/main_frame.html", function() {mainFrameController(model, "view/html/login_ok.html")});
  }
  else {
    if (result.code == USER_NOT_FOUND_ERROR) {
      $("#login-username-form").addClass("is-invalid");
    }
    else if (result.code == PASSWORD_NOT_MATCH_ERROR) {
      $("#login-password-form").addClass("is-invalid");
    }
  }
}

function login(model, username, password, callback) {
  $.ajax({
    url: "api/api.php",
    type: "GET",
    data: {
      "request": "login",
      "username": username,
      "password": password
    },
    success: function(result) {
      callback(model, result);
    }
  });
}
