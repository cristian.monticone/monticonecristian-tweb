function categoriesController(model) {
  apiRequest({
      "request": "get_categories",
    },
    function(result) {
      loadCategoriesTemplate(model, result)
    }
  );
}

function loadCategoriesTemplate(model, result) {
  var cat = result.categories;

  $.get(
    "view/html/template/category_entry.html",
    function(data) {loadCategories(model, cat, data);}
  );
}

function loadCategories(model, cat, tmpl_source) {
  // Caricamento template
  var template = Handlebars.compile(tmpl_source);

  for (i = 0; i < cat.length; i++) {
    var content = template({ name: cat[i].title });
    $("#category-items").append(content);
  }

  $("#category-items p").click(function() {changeCategory(model, $(this).text())});
}

function changeCategory(model, category) {
  $("#middle-frame").load("view/html/product_list.html", function() {loadCategory(model, category)});
}

function loadCategory(model, category) {
  apiRequest({
      "request": "get_products",
      "category": category,
    },
    function(result) {
      loadProductTemplate(model, result);
    }
  );
}
