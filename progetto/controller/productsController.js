function loadProductTemplate(model, result) {
  var prods = result.products;

  $.get(
    "view/html/template/product_list_entry.html",
    function(data) {loadProducts(model, prods, data);}
  );
}

function loadProducts(model, prods, tmpl_source) {
  // Carico il template
  var template = Handlebars.compile(tmpl_source);

  $("#products-grid").empty();
  for (i = 0; i < prods.length; i++) {
    var content = template({
      id: prods[i].id,
      title: prods[i].name,
      img_source: prods[i].image_path,
      price: prods[i].price
    });
    $("#products-grid").append(content);
  }

  $("#products-grid img").draggable({ 
    revert: true,
    containment: "body",
    zIndex: 100
  });
  $("#products-grid > div").click(function() {selectProduct(model, $(this).attr("pid"))});
}

function selectProduct(model, productId) {
  $("#middle-frame").load("view/html/product_page.html", function() {productPageController(model, productId)});
}

function productPageController(model, productId) {
  apiRequest({
      "request": "get_product_info",
      "product_id": productId,
    },
    function(result) {
      initProductPage(model, result, productId);
    }
  );

  $("#add-to-cart-btn").click(function() {addCurrentProductToCart(model, 1)});
}

function initProductPage(model, result, productId) {
  // Aggiorno il modello.
  model.current_p_id = productId;
  model.current_p_price = parseInt(result.price.replace(".",""));
  model.current_p_title = result.name;

  if(model.login && !result.voted)
    $("#review-form-container").load("view/html/review_form.html", function() {reviewFormController(model, result, productId)});

  $("#product-image").attr("src", result.image_path)
  $("#product-name").text(result.name);
  $("#product-desc").text(result.description);

  if (result.avg_vote) {
    $("#product-vote > span").text(result.avg_vote);
    $("#product-vote").show();
  }

  $("#product-price").text(result.price);

  var revs = result.reviews;
  if (revs.length == 0)
    $("#product-reviews").load("view/html/no_reviews.html");
  else
    loadReviewTemplate(model, revs)

  if (revs.length == 0)
    $("#product-reviews").load("view/html/no_reviews.html");
}

function loadReviewTemplate(model, revs) {
  $.get(
    "view/html/template/product_review_entry.html",
    function(data) {loadReviews(model, revs, data);}
  );
}

function loadReviews(model, revs, tmpl_source) {
  // Carico il template
  var template = Handlebars.compile(tmpl_source);
    
  for (i = 0; i < revs.length; i++) {
    var content = template({
      username: revs[i].username,
      vote: revs[i].vote,
      text: revs[i].text
    });
    $("#product-reviews").append(content);
  }
}

function reviewFormController(model, result, productId) {
  $("#review-form").submit(function(e) {
    e.preventDefault();
    insertReview(model, 
          this.elements['text'].value,
          this.elements['vote'].value,
          productId);
  });
}

function insertReview(model, text, vote, productId) {
  apiRequest({
      "request": "insert_review",
      "product_id": productId,
      "text": text,
      "vote": vote
    },
    function(result) {
      if (result.ok) {
        $("#middle-frame").load("view/html/product_page.html", function() {productPageController(model, productId)});
      }
      else if (result.error = DEINED_REQUEST_ERROR) {
        $("#user-nav-box").load("view/html/login_nav_box.html",
                                function() {loginBoxController(model)});
        $("#main-frame").load(
          "view/html/main_frame.html", 
          function() {mainFrameController(model, "view/html/not_logged.html");}
        );
      }
    }
  );
}
