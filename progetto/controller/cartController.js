function cartController(model) {
  // Caricamento cart status e totale
  var cart_lines = Cookies.getJSON('cart_lines');
  var total_cents = parseInt(Cookies.get('total_cents'));
  cartRefresh(model, cart_lines, total_cents);

  $("#shopping-cart-frame").droppable({ drop: function(event, ui) {
    var id = ui.draggable.parent().attr("pid");
    var title = ui.draggable.parent().find("p").text();
    var price = parseInt(ui.draggable.parent().find("span").text().replace(".",""));
    addProductToCart(model, id, title, price, 1);
  }});

  if (model.login) {
    $("#buy-order-btn").click(function() {
      $("#main-frame").load("view/html/order.html", function() {orderController(model)});
    });
  }
  else {
    $("#buy-order-msg").text("Per poter completare l'acquisto devi effettuare l'accesso.");
  }
}

function cartRefresh(model, cart_lines, total_cents) {
  $("#cart-rows").empty();

  if (!cart_lines || cart_lines.length == 0) {
    $("#cart-rows").load("view/html/empty_cart.html");
    $("#cart-total").text(0);
    $("#buy-order-btn").hide();
  }
  else {
    if (model.login) 
      $("#buy-order-btn").show();
    
    loadCartEntryTemplate(model, cart_lines);
    $("#cart-total").text(total_cents/100);
  }
}

function loadCartEntryTemplate(model, cart_lines) {
  $.get(
    "view/html/template/cart_entry.html",
    function(data) {loadCartLines(model, cart_lines, data);}
  );
}

function loadCartLines(model, cart_lines, tmpl_source) {
  // Carico il template
  var template = Handlebars.compile(tmpl_source);

  for (i = 0; i < cart_lines.length; i++) {
    var content = template({
      id: cart_lines[i].id,
      title: cart_lines[i].title,
      number: cart_lines[i].numero,
      price: (cart_lines[i].prezzo * cart_lines[i].numero / 100)
    });
    $("#cart-rows").append(content);
  }


  // Listeners del tasto per rimuovere una entry dal proprio carrello.
  $(".remove-cart-btn").click(function() {removeProductFromCart(model, $(this).attr("pid"))});

  $(".cart-number-forms").on("mouseup keyup", function() {updateProductCart(model, $(this))});
}

function addCurrentProductToCart(model, number) {
  addProductToCart(model, 
                   model.current_p_id, 
                   model.current_p_title, 
                   model.current_p_price, 
                   number);
}

function addProductToCart(model, productId, title, price, number) {
  var cart_lines = Cookies.getJSON('cart_lines');
  var total_cents = parseInt(Cookies.get('total_cents'));

  if (!cart_lines)
    cart_lines = new Array();

  var flag = true;
  for (i = 0; flag && i < cart_lines.length; i++) {
    if (cart_lines[i].id === productId) {
      cart_lines[i].numero += number;
      flag = false;
    }
  }

  if (flag) {
    cart_lines.push({
      "id": productId, 
      "title": title, 
      "prezzo": price,
      "numero": number
    });
  }

  if (!total_cents) total_cents = 0;
  total_cents += (price * number);
  Cookies.set('total_cents', total_cents);

  Cookies.set('cart_lines', cart_lines);
  cartRefresh(model, cart_lines, total_cents)
}

function updateProductCart(model, form) {
  var productId = parseInt(form.attr("pid"));
  var number = parseInt(form.val());
  var cart_lines = Cookies.getJSON('cart_lines');
  var total_cents = parseInt(Cookies.get('total_cents'));

  if (!cart_lines)
    return; // Errore: si sta cercando di cambiare numero su un carrello vuoto.

  if (isNaN(number))
    return; // Numero non valido.

  var flag = true;
  var oldNumber, price;
  for (i = 0; flag && i < cart_lines.length; i++) {
    if (cart_lines[i].id == productId) {
      oldNumber = cart_lines[i].numero;
      cart_lines[i].numero = number;
      price = cart_lines[i].prezzo;
      flag = false;
    }
  }

  var delta = number - oldNumber;

  if (!total_cents) total_cents = 0;
  total_cents += (price * delta);
  form.parents().eq(2).find(".cart-price").text(price * number / 100);
  $("#cart-total").text(total_cents/100);
  Cookies.set('total_cents', total_cents);
  Cookies.set('cart_lines', cart_lines);
}

function removeProductFromCart(model, productId) {
  var cart_lines = Cookies.getJSON('cart_lines');
  var total_cents = parseInt(Cookies.get('total_cents'));

  if (!cart_lines) return;  // Cenrcando di eliminare qualcosa da un carrello vuoto.

  var flag = true;
  for (i = 0; flag && i < cart_lines.length; i++) {
    if (cart_lines[i].id == productId) {
      total_cents -= (cart_lines[i].prezzo * cart_lines[i].numero);
      cart_lines.splice(i, 1);
    }
  }

  Cookies.set('total_cents', total_cents);
  Cookies.set('cart_lines', cart_lines);
  cartRefresh(model, cart_lines, total_cents)
}

