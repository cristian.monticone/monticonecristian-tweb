function registerController(model) {
  model.main_frame = REGISTER_FRAME;

  $("#register-username-control").on("change paste keyup", function() {checkUsernameControl($(this).val())});
  $("#register-mail-control").on("change paste keyup", function() {checkMailControl($(this).val())});
  $("#register-form").submit(function(e) {
    e.preventDefault();
    register(model, 
          this.elements['mail'].value,
          this.elements['username'].value,
          this.elements['name'].value,
          this.elements['surname'].value,
          this.elements['password'].value,
          handleRegisterResult);
  });
}

function checkUsernameControl(val) {
  if (val.length >=5 && val.length <= 15) {
    apiRequest({
        "request": "user_exist",
        "username": val,
      },
      function(result) {
        if (result.ok) {
          $("#register-username-form .invalid-feedback").text("Nome utente già utilizzato.");
          $("#register-username-control").removeClass("is-valid").addClass("is-invalid");
        }
        else if (result.code == USER_NOT_FOUND_ERROR) {
          $("#register-username-form .valid-feedback").text("Username libero.");
          $("#register-username-control").removeClass("is-invalid").addClass("is-valid");
        }
        else {
          $("#register-username-form .valid-feedback").text(result.error);
          $("#register-username-control").removeClass("is-valid").addClass("is-invalid");
        }
      }
    );
  }
  else {
    $("#register-username-control").removeClass("is-valid is-invalid")
  }
}

function checkMailControl(val) {
  if (val.length >=5 && val.length <= 64) {
    apiRequest({
        "request": "mail_exist",
        "mail": val,
      },
      function(result) {
        if (result.ok) {
          $("#register-mail-form .invalid-feedback").text("Indirizzo email già utilizzato.");
          $("#register-mail-control").removeClass("is-valid").addClass("is-invalid");
        }
        else if (result.code == MAIL_NOT_FOUND_ERROR) {
          $("#register-mail-form .valid-feedback").text("Indirizzo email libero.");
          $("#register-mail-control").removeClass("is-invalid").addClass("is-valid");
        }
        else {
          $("#register-mail-form .invalid-feedback").text(result.error);
          $("#register-mail-control").removeClass("is-valid").addClass("is-invalid");
        }
      }
    );
  }
  else {
    $("#register-mail-control").removeClass("is-valid is-invalid");
  }
}

function handleRegisterResult(model, result) {
  $("#register-form input").removeClass("is-invalid");

  if (result.ok) {
    $("#main-frame").load("view/html/main_frame.html", function() {mainFrameController(model, "view/html/register_ok.html")});
  }
  else {
    if (result.code == DUPLICATE_MAIL_ERROR) {
      $("#register-mail-form .invalid-feedback").text("Indirizzo email già utilizzato.");
      $("#register-mail-control").removeClass("is-valid").addClass("is-invalid");
    }
    else if (result.code == DUPLICATE_USER_ERROR) {
      $("#register-username-form .invalid-feedback").text("Nome utente già utilizzato.");
      $("#register-username-control").removeClass("is-valid").addClass("is-invalid");
    }
  }
}

function register(model, mail, username, name, surname, password, callback) {
  apiRequest({
      "request": "register",
      "mail" : mail,
      "username": username,
      "name" : name,
      "surname" : surname,
      "password": password
    },
    function(result) {
      callback(model, result);
    }
  );
}
