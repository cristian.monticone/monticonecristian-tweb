<?php
  defined("TOKEN") OR die("Acesso illegittimo ad uno script interno.");

  /**
   *  Restituisce un array contenente i titoli e gli id delle categorie.
   */
  function handleGetCategories() : array {
    try {
      $dbh = initPdo();
      $pdostatement = $dbh->prepare("SELECT * FROM category ORDER BY title;");
      $success = $pdostatement->execute();
    }
    catch (PDOexception $e) {
      // L'errore del database viene inviato solo se la modalità debug è attiva.
      return internal_error($e->getMessage());
    }

    // L'errore del database viene inviato solo se la modalità debug è attiva.
    if (!$success)
      return internal_error($pdostatement->errorInfo()[2]);

    $res = $pdostatement->fetchAll(PDO::FETCH_ASSOC);

    return array(
      'ok' => true,
      'categories' => $res
    );
  }

  /**
   *  Prende in input l'id di un prodotto e restituisce un array con le
   *  sue informazioni.
   */
  function handleGetProductInfo() : array {
    try {
      $input = init_input("product_id");
    }
    catch (EmptyInputException $e) {
      return error($e->getMessage(), INVALID_INPUT_ERROR);
    }

    try {
      $dbh = initPdo();
      
      $pdostatement = $dbh->prepare("SELECT * FROM product WHERE id = :product_id ;");
      $pdostatement->bindValue(':product_id', $input['product_id'], PDO::PARAM_INT);
      $success = $pdostatement->execute();
    }
    catch (PDOexception $e) {
      // L'errore del database viene inviato solo se la modalità debug è attiva.
      return internal_error($e->getMessage());
    }

    $res = $pdostatement->fetch(PDO::FETCH_ASSOC);

    // L'errore del database viene inviato solo se la modalità debug è attiva.
    if (!$success)
      return internal_error($pdostatement->errorInfo()[2]);

    try {
      // Calcolo il voto medio per il prodotto.
      $pdostatement = $dbh->prepare("SELECT AVG(vote) AS avg_vote FROM review WHERE product = :product_id;");
      $pdostatement->bindValue(':product_id', $input['product_id'], PDO::PARAM_INT);
      $success = $pdostatement->execute();

      // L'errore del database viene inviato solo se la modalità debug è attiva.
      if (!$success)
        return internal_error($pdostatement->errorInfo()[2]);

      $aux = $pdostatement->fetch(PDO::FETCH_ASSOC);
      $res['avg_vote'] = round($aux['avg_vote'], 2);

      if (!checkSession()) {
        // Controllo se l'utente ha già recensito questo prodotto.
        $pdostatement = $dbh->prepare("SELECT * FROM review WHERE product = :product_id AND user = :user_id;");
        $pdostatement->bindValue(':product_id', $input['product_id'], PDO::PARAM_INT);
        $pdostatement->bindValue(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);
        $success = $pdostatement->execute();
      
        // L'errore del database viene inviato solo se la modalità debug è attiva.
        if (!$success)
          return internal_error($pdostatement->errorInfo()[2]);

        $res['voted'] = ($pdostatement->rowCount() == 1);
      }
      else {
        $res['voted'] = false;
      }
    }
    catch (PDOexception $e) {
      // L'errore del database viene inviato solo se la modalità debug è attiva.
      return internal_error($e->getMessage());
    }
    
    // Recensioni sul prodotto.
    try {
      $pdostatement = $dbh->prepare(
        "SELECT u.id, u.username, u.name, u.surname, r.text, r.vote " .
        "FROM product p JOIN review r ON (p.id = r.product) " .
        "  JOIN user u ON (u.id = r.user) " .
        "WHERE p.id = :product_id " .
        "ORDER BY r.submission DESC;"
      );

      $pdostatement->bindValue(':product_id', $input['product_id'], PDO::PARAM_INT);

      $success = $pdostatement->execute();
    }
    catch (PDOexception $e) {
      // L'errore del database viene inviato solo se la modalità debug è attiva.
      return internal_error($e->getMessage());
    }

    // L'errore del database viene inviato solo se la modalità debug è attiva.
    if (!$success)
      return internal_error($pdostatement->errorInfo()[2]);

    $res['reviews'] = $pdostatement->fetchAll(PDO::FETCH_ASSOC);

    return $res;
  }

  /**
   *  Inserisce una recensione ad un prodotto.
   */
  function handleInsertReview() : array {
    try {
      $input = init_input("product_id", "text", "vote");
    }
    catch (EmptyInputException $e) {
      return error($e->getMessage(), INVALID_INPUT_ERROR);
    }

    if (!checkSession())
      return error("Nessuna sessione attiva.", DEINED_REQUEST_ERROR);

    // Validazione input.
    if (strlen($input['text']) > 1024) return error("Recensione troppo lunga.", INVALID_INPUT_ERROR);
    if ($input['vote'] > 5) return error("Voto non valido.", INVALID_INPUT_ERROR);
    if ($input['vote'] < 1) return error("Voto non valido.", INVALID_INPUT_ERROR);

    try {
      $dbh = initPdo();
      
      $pdostatement = $dbh->prepare("INSERT INTO review(product, user, text, vote) " .
                                    "VALUES(:product_id, :username_id, :text, :vote);");

      $pdostatement->bindValue(':product_id', $input['product_id'], PDO::PARAM_INT);
      $pdostatement->bindValue(':username_id', $_SESSION['user_id'], PDO::PARAM_INT);
      $pdostatement->bindValue(':text', $input['text'], PDO::PARAM_STR);
      $pdostatement->bindValue(':vote', $input['vote'], PDO::PARAM_STR);

      $success = $pdostatement->execute();
    }
    catch (PDOexception $e) {
      // L'errore del database viene inviato solo se la modalità debug è attiva.
      return internal_error($e->getMessage());
    }

    // L'errore del database viene inviato solo se la modalità debug è attiva.
    if (!$success)
      return internal_error($pdostatement->errorInfo()[2]);

    // Tupla inserita!
    return array('ok' => true);
  }

  /**
   *  Data una query di ricerca restituisce un array con tutti i prodotto
   *  collegati alla ricerca.
   */
  function handleSearchProducts() : array {
    try {
      $input = init_input("query");
    }
    catch (EmptyInputException $e) {
      return error($e->getMessage(), INVALID_INPUT_ERROR);
    }

    $words = explode(" ", $input['query']);

    $query = "SELECT * FROM product WHERE ";
    for ($i = 0; $i < count($words); $i++) {
      if ($i > 0) 
        $query .= " AND ";
      $query .= " name LIKE '%" . $words[$i] . "%'";
    }
    $query .= ";";

    try {
      $dbh = initPdo();
      $pdostatement = $dbh->prepare($query);

      $success = $pdostatement->execute();
    }
    catch (PDOexception $e) {
      // L'errore del database viene inviato solo se la modalità debug è attiva.
      return internal_error($e->getMessage());
    }

    // L'errore del database viene inviato solo se la modalità debug è attiva.
    if (!$success)
      return internal_error($pdostatement->errorInfo()[2]);

    $res = $pdostatement->fetchAll(PDO::FETCH_ASSOC);

    return array(
      'ok' => true,
      'products' => $res
    );
  }
  
  /**
   *  Data una categoria restituisce l'array con i sui prodotti.
   */
  function handleGetProducts() : array {
    try {
      $input = init_input("category");
    }
    catch (EmptyInputException $e) {
      return error($e->getMessage(), INVALID_INPUT_ERROR);
    }

    try {
      $dbh = initPdo();
      $pdostatement = $dbh->prepare("SELECT p.* FROM product p JOIN category c ON(c.id = p.category) " .
                                    "WHERE c.title = :title ;");

      $pdostatement->bindValue(':title', $input['category'], PDO::PARAM_STR);

      $success = $pdostatement->execute();
    }
    catch (PDOexception $e) {
      // L'errore del database viene inviato solo se la modalità debug è attiva.
      return internal_error($e->getMessage());
    }

    // L'errore del database viene inviato solo se la modalità debug è attiva.
    if (!$success)
      return internal_error($pdostatement->errorInfo()[2]);

    $res = $pdostatement->fetchAll(PDO::FETCH_ASSOC);

    return array(
      'ok' => true,
      'products' => $res
    );
  }

  /**
   *  Inserisce nel database un'ordine di un utente.
   */
  function handleInsertOrder() : array {
    $json = $_REQUEST["ordered_products"];
    if (empty($json)) 
      return error("Nessun prodotto.", INVALID_INPUT_ERROR);
    $cart_lines = json_decode($json, true);

    try {
      $input = init_input("address", "card_code", "ccv", "payment_method", "expiration");
    }
    catch (EmptyInputException $e) {
      return error($e->getMessage(), INVALID_INPUT_ERROR);
    }

    if (!checkSession())
      return error("Nessuna sessione attiva.", DEINED_REQUEST_ERROR);

    // Validazione input.
    if (strlen($input["address"]) < 5) return error("Indirizzo troppo corto.", INVALID_INPUT_ERROR);
    if (strlen($input["address"]) > 64) return error("Indirizzo troppo lungo.", INVALID_INPUT_ERROR);
    if (strlen($input["card_code"]) != 16) return error("Il numero della carta non è formato da 16 cifre.", INVALID_INPUT_ERROR);
    if (strlen($input["ccv"]) != 3) return error("Il CCV non è formato da 3 cifre.", INVALID_INPUT_ERROR);

    if ($input["payment_method"] != "Visa" &&
        $input["payment_method"] != "Mastercard" &&
        $input["payment_method"] != "Maestro")
          return error("Metodo di pagamento non valido.", INVALID_INPUT_ERROR);

    try {
      $dbh = initPdo();
      $pdostatement = $dbh->prepare("INSERT INTO orders(user, address) VALUES (:user_id, :address);");

      $pdostatement->bindValue(':user_id', $_SESSION["user_id"], PDO::PARAM_INT);
      $pdostatement->bindValue(':address', $input["address"], PDO::PARAM_STR);

      $success = $pdostatement->execute();
    }
    catch (PDOexception $e) {
      // L'errore del database viene inviato solo se la modalità debug è attiva.
      return internal_error($e->getMessage());
    }

    // L'errore del database viene inviato solo se la modalità debug è attiva.
    if (!$success)
      return internal_error($pdostatement->errorInfo()[2]);

    $order_id = $dbh->lastInsertId();

    foreach ($cart_lines as $item) {
      try {
        $pdostatement = $dbh->prepare("INSERT INTO order_product(order_id, product_id, number) " .
                                      "VALUES (:order_id, :product_id, :number);");

        $pdostatement->bindValue(':order_id', $order_id, PDO::PARAM_INT);
        $pdostatement->bindValue(':product_id', htmlspecialchars($item["id"]), PDO::PARAM_INT);
        $pdostatement->bindValue(':number', htmlspecialchars($item["numero"]), PDO::PARAM_INT);

        $success = $pdostatement->execute();
      }
      catch (PDOexception $e) {
        // L'errore del database viene inviato solo se la modalità debug è attiva.
        return internal_error($e->getMessage());
      }

      // L'errore del database viene inviato solo se la modalità debug è attiva.
      if (!$success)
        return internal_error($pdostatement->errorInfo()[2]);
    }

    return array('ok' => true);
  }

?>
