<?php
  defined("TOKEN") OR die("Acesso illegittimo ad uno script interno.");

  /**
   *  Inizializza un oggeto PDO con i valori di configurazione e lo
   *  restituisce al chiamante.
   */
  function initPdo() : PDO {
    return new PDO("mysql:host={$GLOBALS['db_conf']['host']};dbname={$GLOBALS['db_conf']['name']};charset=utf8",
                   $GLOBALS['db_conf']['user'],
                   $GLOBALS['db_conf']['password']);
  }
?>
