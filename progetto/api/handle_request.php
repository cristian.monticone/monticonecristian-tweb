<?php
  defined("TOKEN") OR die("Acesso illegittimo ad uno script interno.");

  require 'database.php';
  require 'error.php';
  require 'user.php';
  require 'shop.php';

  // Dispacher delle richieste.
  function handleRequest($request) {
    if ($request == "login")
      return handleLoginRequest();
    else if ($request == "register")
      return handleRegisterRequest();
    else if ($request == "logout")
      return handleLogoutRequest();
    else if ($request == "session_check")
      return handleSessionCheckRequest();
    else if ($request == "user_exist")
      return handleUserExist();
    else if ($request == "mail_exist")
      return handleMailExist();
    else if ($request == "get_categories")
      return handleGetCategories();
    else if ($request == "get_products")
      return handleGetProducts();
    else if ($request == "get_product_info")
      return handleGetProductInfo();
    else if ($request == "search_products")
      return handleSearchProducts();
    else if ($request == "insert_review")
      return handleInsertReview();
    else if ($request == "insert_order")
      return handleInsertOrder();
    else
      return handleUnknownRequest($request);
  }

  function handleUnknownRequest(String $request) : array {
    if (empty($request))
      $error = "Richiesta vuota non valida.";
    else
      $error = "Richiesta '$request' non valida.";

    return error($error, INVALID_REQUEST_ERROR);
  }

  function notImplementedYet() : array {
    return error("Richiesta non ancora implementata.", NOT_YET_IMPLEMENTED_ERROR);
  }

  function init_input(String ...$args) : array {
    $dic = array();
    $empty_vars = "";

    foreach ($args as $var) {
      $dic[$var] = htmlspecialchars(@$_REQUEST[$var]);
    
      if (empty($dic[$var]))
        $empty_vars .= "'$var'; ";
    }

    if (!empty($empty_vars))
        throw new EmptyInputException("I campi ($empty_vars) sono vuoti.");

    return $dic;
  }
?>
