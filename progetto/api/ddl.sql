CREATE TABLE user (
    id INT AUTO_INCREMENT,
    username VARCHAR(64) NOT NULL UNIQUE,
    name VARCHAR(64) NOT NULL,
    surname VARCHAR(64) NOT NULL,
    password_hash TEXT NOT NULL,
    avatar_path VARCHAR(128),
    mail_address VARCHAR(128) UNIQUE,
    PRIMARY_KEY(id)
);

CREATE TABLE category (
    id INT AUTO_INCREMENT,
    title VARCHAR(32) NOT NULL UNIQUE,
    PRIMARY KEY(id)
);

CREATE TABLE product (
  id INT AUTO_INCREMENT,
  name VARCHAR(64) NOT NULL,
  description VARCHAR(1024) NOT NULL,
  image_path VARCHAR(256) NOT NULL,
  price DECIMAL(15, 2) NOT NULL,
  category INT NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY(category) REFERENCES category(id)
);

CREATE TABLE review (
    product INT,
    user INT,
    text VARCHAR(1024) NOT NULL,
    vote TINYINT NOT NULL,
    submission TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY(product, user),
    FOREIGN KEY(product) REFERENCES product(id),
    FOREIGN KEY(user) REFERENCES user(id),
    CHECK (vote > 0),
    CHECK (vote <= 5)
);

REATE TABLE orders (
    id INT AUTO_INCREMENT,
    user INT NOT NULL,
    address VARCHAR(256) NOT NULL,
    timestamp TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY(id),
    FOREIGN KEY(user) REFERENCES user(id)
);

CREATE TABLE order_product (
    order_id INT,
    product_id INT,
    number INT NOT NULL,
    PRIMARY KEY(product_id, order_id),
    FOREIGN KEY(order_id) REFERENCES orders(id),
    FOREIGN KEY(product_id) REFERENCES product(id),
    CHECK (number > 0)
);
