<?php
  defined("TOKEN") OR die("Acesso illegittimo ad uno script interno.");

  // Codici di errore.
  const GENERIC_ERROR             = 1;
  const USER_NOT_FOUND_ERROR      = 2;
  const PASSWORD_NOT_MATCH_ERROR  = 3;
  const INVALID_REQUEST_ERROR     = 4;
  const NOT_YET_IMPLEMENTED_ERROR = 5;
  const INVALID_INPUT_ERROR       = 6;
  const INTERNAL_SERVER_ERROR     = 7;
  const DUPLICATE_USER_ERROR      = 8;
  const DUPLICATE_MAIL_ERROR      = 9;
  const NO_ACTIVE_SESSION_ERROR   = 10;
  const MAIL_NOT_FOUND_ERROR      = 11;
  const DEINED_REQUEST_ERROR      = 12;

  /**
   *  Restituisce un array contente un messaggio e un codice di errore.
   */
  function error($message, $code = GENERIC_ERROR) : array {
    return array(
      'ok' => false,
      'code' => $code,
      'error' => $message
    );
  }

  /**
   *  Restituisce un errore interno al server. Se la modalità debug è attiva
   *  restituisce le informazioni dettagliate, le maschera altrimenti.
   */
  function internal_error(String $message) : array {
    if ($GLOBALS['conf']['debug'])
      return error("Internal error: " . $message, INTERNAL_SERVER_ERROR);
    else
      return error("Internal server error.", INTERNAL_SERVER_ERROR);
  }

  /**
   *  Eccezzione lanciato quando un input necessario è vuoto.
   */
  class EmptyInputException extends Exception {}
?>
