<?php
  define("TOKEN", true);

  require 'config.php';
  require 'handle_request.php';

  $request = htmlspecialchars(@$_REQUEST['request']);
  $res_arr = handleRequest($request);

  header("Content-type: application/json; charset=utf-8");
  print json_encode($res_arr, JSON_UNESCAPED_UNICODE); // JSON_UNESCAPED_UNICODE: per codificare correttamente la codifica utf-8
?>
