<?php
  defined("TOKEN") OR die("Acesso illegittimo ad uno script interno.");

  /**
   *  Gestisce la richiesta di login di un utente e ne imposta la sessione.
   */
  function handleLoginRequest() : array {
    try {
      $input = init_input("username", "password");
    }
    catch (EmptyInputException $e) {
      return error($e->getMessage(), INVALID_INPUT_ERROR);
    }

    // Aggiungo il 'sale' alla password.
    $input['password'] .= $GLOBALS['db_conf']['salt'];

    $input['username'] = strtolower($input['username']);
    $input['mail'] = strtolower($input['mail']);

    try {
      $dbh = initPdo();
      $pdostatement = $dbh->prepare("SELECT * FROM user WHERE username = :username ;");
      $pdostatement->bindValue(':username', $input['username'], PDO::PARAM_STR);

      $success = $pdostatement->execute();
    }
    catch (PDOexception $e) {
      // L'errore del database viene inviato solo se la modalità debug è attiva.
      return internal_error($e->getMessage());
    }

    // L'errore del database viene inviato solo se la modalità debug è attiva.
    if (!$success)
      return internal_error($pdostatement->errorInfo()[2]);

    if ($pdostatement->rowCount() == 0)
      return error("L'utente {$input['username']} non esiste.", USER_NOT_FOUND_ERROR);

    $result = $pdostatement->fetch(PDO::FETCH_ASSOC);
    if (password_verify($input['password'], $result['password_hash'])) {
      session_start();
      $_SESSION['username'] = $input['username'];
      $_SESSION['user_id'] = $result['id'];
      return array('ok' => true, 'username' => $input['username']);
    }
    else {
      return error("Password inserita non corretta.", PASSWORD_NOT_MATCH_ERROR);
    }
  }

  /**
   *  Verifica che il client abbia una sessione di login valida.
   */
  function handleSessionCheckRequest() : array {
    session_start();
    if (empty($_SESSION['username']))
      return error("Nessuna sessiona attiva.", NO_ACTIVE_SESSION_ERROR);
    else
      return array('ok' => true, 'username' => $_SESSION['username']);
  }
  
  /**
   *  Controlla se un utente esiste.
   */
  function handleUserExist() : array {
    try {
      $input = init_input("username");
    }
    catch (EmptyInputException $e) {
      return error($e->getMessage(), INVALID_INPUT_ERROR);
    }

    $input['username'] = strtolower($input['username']);
  
    try {
      $dbh = initPdo();
      $pdostatement = $dbh->prepare("SELECT * FROM user WHERE username = :username ;");
      $pdostatement->bindValue(':username', $input['username'], PDO::PARAM_STR);

      $success = $pdostatement->execute();
    }
    catch (PDOexception $e) {
      // L'errore del database viene inviato solo se la modalità debug è attiva.
      return internal_error($e->getMessage());
    }

    // L'errore del database viene inviato solo se la modalità debug è attiva.
    if (!$success)
      return internal_error($pdostatement->errorInfo()[2]);

    if ($pdostatement->rowCount() == 0)
      return error("L'utente {$input['username']} non esiste.", USER_NOT_FOUND_ERROR);
    else
      return array('ok' => true);
  }

  /**
   *  Controlla se un indirizzo mail è già utilizzato.
   */
  function handleMailExist() : array {
    try {
      $input = init_input("mail");
    }
    catch (EmptyInputException $e) {
      return error($e->getMessage(), INVALID_INPUT_ERROR);
    }
    
    $input['mail'] = strtolower($input['mail']);

    if (!filter_var($input['mail'], FILTER_VALIDATE_EMAIL))
      return error("Indirizzo email non valido.", INVALID_INPUT_ERROR);
  
    try {
      $dbh = initPdo();
      $pdostatement = $dbh->prepare("SELECT * FROM user WHERE mail_address = :mail ;");
      $pdostatement->bindValue(':mail', $input['mail'], PDO::PARAM_STR);

      $success = $pdostatement->execute();
    }
    catch (PDOexception $e) {
      // L'errore del database viene inviato solo se la modalità debug è attiva.
      return internal_error($e->getMessage());
    }

    // L'errore del database viene inviato solo se la modalità debug è attiva.
    if (!$success)
      return internal_error($pdostatement->errorInfo()[2]);

    if ($pdostatement->rowCount() == 0)
      return error("L'indirizzo mail {$input['mail']} non esiste.", MAIL_NOT_FOUND_ERROR);
    else
      return array('ok' => true);
  }

  function handleRegisterRequest() : array {
    try {
      $input = init_input("username", "password", "name", "surname", "mail");
    }
    catch (EmptyInputException $e) {
      return error($e->getMessage(), INVALID_INPUT_ERROR);
    }

    $input['username'] = strtolower($input['username']);
    $input['mail'] = strtolower($input['mail']);

    if (!filter_var($input['mail'], FILTER_VALIDATE_EMAIL))
      return error("Indirizzo email non valido.", INVALID_INPUT_ERROR);

    // Validazione lunghezze input
    if (strlen($input['username']) < 5) return error("Username troppo corto.", INVALID_INPUT_ERROR);
    if (strlen($input['username']) > 15) return error("Username troppo lungo.", INVALID_INPUT_ERROR);
    if (strlen($input['mail']) < 5) return error("Mail troppo corta.", INVALID_INPUT_ERROR);
    if (strlen($input['mail']) > 64) return error("Mail troppo lunga.", INVALID_INPUT_ERROR);
    if (strlen($input['name']) < 5) return error("Nome troppo corto.", INVALID_INPUT_ERROR);
    if (strlen($input['name']) > 15) return error("Nome troppo lungo.", INVALID_INPUT_ERROR);
    if (strlen($input['surname']) < 5) return error("Cognome troppo corto.", INVALID_INPUT_ERROR);
    if (strlen($input['surname']) > 15) return error("Cognome troppo lungo.", INVALID_INPUT_ERROR);
    if (strlen($input['password']) < 8) return error("Password troppo corta.", INVALID_INPUT_ERROR);
    if (strlen($input['password']) > 128) return error("Password troppo lunga.", INVALID_INPUT_ERROR);

    // Aggiungo il 'sale' alla password.
    $input['password'] .= $GLOBALS['db_conf']['salt'];

    // Applico l'algoritmo di hash Argon2id sulla password.
    $input['password'] = password_hash($input['password'], PASSWORD_ARGON2ID);

    try {
      $dbh = initPdo();
      $pdostatement = $dbh->prepare("INSERT INTO user(username, password_hash, name, surname, mail_address) " .
                                    "VALUES (:username, :password, :name , :surname, :mail);");

      $success = $pdostatement->execute($input);
    }
    catch (PDOexception $e) {
      // L'errore del database viene inviato solo se la modalità debug è attiva.
      return internal_error($e->getMessage());
    }

    if (!$success) {
      $error = $pdostatement->errorInfo();

      if ($error[1] == 1062) { // Entry già esistente => utente o mail già esistente.
        if ($error[2] == "Duplicate entry '{$input['username']}' for key 'username'")
          return error("L'utente '{$input['username']}' è già esistente.", DUPLICATE_USER_ERROR);
        else if ($error[2] == "Duplicate entry 'cristian@cristian.it' for key 'mail_address'")
          return error("L'indirizzo email '{$input['mail']}' è già utilizzato.", DUPLICATE_MAIL_ERROR);
        else
          return internal_error($error[2]);
      }
      else {
        return internal_error($error[2]);
      }
    }
    else {
      return array('ok' => true);
    }
  }

  /**
   *  Gestisce la richiesta di logout del client disattivando la sua sessione.
   */
  function handleLogoutRequest() : array {
    session_start();
    session_unset();
    session_destroy();

    return array('ok' => true);
  }

  /**
   *  Restituisce true se il client ha una sessione attiva, false altrimenti.
   */
  function checkSession() : bool {
    session_start();
    return (!empty($_SESSION['username']));
  }
?>
